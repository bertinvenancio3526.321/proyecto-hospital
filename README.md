Redacción generalizada
Las historias de usuario deben de ser precisas y detalladas.
La redacción debe de tener las características:
I    Independiente
N    Negociable
V    Valiosa
E    Estimable
S    Pequeña
T    Testeable
Hay que comenzar el desglose de HU a partir de:

Los objetos involucrados en el sistema
Los diferentes perfiles
CRUD (Funcionalidades principales)

Tomó las necesidades generales planteadas por el proyecto y las puso en HU pero no plasma acciones del sistema en sí solo la redacción general.

Por  ejemplo en la hu de laboratorio

*El generar la orden es una acción diferente*

El laboratorista deberá generar una orden en la cual se muestren los datos del paciente.


*generar diagnóstico es otra funcionalidad con características totalmente diferentes*

 Después deberá generar un diagnostico en el cual explica la enfermedad del paciente y describir  si a mejorado o a empeorado, así mismo describiendo la causa de la enfermedad.

*El envío del diagnóstico al paciente es otra funcionalidad diferente*

 A continuación deberá revisar el registro del paciente para poder obtener el correo electrónico y numero de teléfono para poder enviar el diagnostico de laboratorio. Ver en:


Pacientes

*Esta es otra funcionalidad*
 El laboratorista deberá acudir a el registro de los doctores para poder obtener el correo electrónico de el doctor que consulto el paciente. Ver en:
Doctores


Al ser funcionalidades totalmente diferentes las convierte en historias de usuario independientes que no deben estar redactadas en una misma.
